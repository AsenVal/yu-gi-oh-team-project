﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MathTaskLibrary;
using MathTaskLibrary.Arithmetic;
using MathTaskLibrary.Geometry;
using MathTaskLibrary.Other;


namespace LearnMath
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        public static IRenderer render = new WPFRenderer();
        public Engine exam;
        public Difficulty TaskLevel = Difficulty.normal;
        public int taskCount = 10;

		public MainWindow()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}

		private void Minimize(object sender, RoutedEventArgs e)
        {

            this.WindowState = WindowState.Minimized;

        }

		private void Close_Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Close();
		}

		private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			this.DragMove();
		}

        private void Minimize_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;

        }
			

		private void ComboBoxDifficulty_DropDownClosed(object sender, System.EventArgs e)
		{
            if (ComboBoxDifficulty.Text == "Difficult")
            {
                TaskLevel = Difficulty.difficult;
            }
            else
            {
                TaskLevel = Difficulty.normal;
            }
                
		}
        //private void ExamWorking(MathTask task)
        //{
        //        exam.ChooseExam(task);
        //        
        //}

		private void AdditionFromButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            
            exam = new Engine(render);
            exam.CreateTasks(typeof(AdditionalTaskFrom), TaskLevel, taskCount);
            exam.Run();
		}

		private void AdditionByButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(AdditionalTaskBy), TaskLevel, taskCount);
            exam.Run();
		}

		private void AdditionOperatorsButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(AdditionalOperators), TaskLevel, taskCount);
            exam.Run();
		}

		private void SubstractionByButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(SubstractionalTaskBy), TaskLevel, taskCount);
            exam.Run();
		}

		private void SubstractionFromButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(SubstractionalTaskFrom), TaskLevel, taskCount);
            exam.Run();
		}

		private void SubstractionOperatorsButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(SubstractionOperators), TaskLevel, taskCount);
            exam.Run();
		}

		private void MultiplyByButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(MultiplicationalTaskBy), TaskLevel, taskCount);
            exam.Run();
		}

		private void MultiplyFromButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(MultiplicationalTaskFrom), TaskLevel, taskCount);
            exam.Run();
		}

		private void MultiplyOperatorsButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(MultiplicationalOperatos), TaskLevel, taskCount);
            exam.Run(); ;
		}

		private void DivisionByButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(DivisionTaskBy), TaskLevel, taskCount);
            exam.Run();
		}

		private void DivisionTaskFromButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(DivisionTaskFrom), TaskLevel, taskCount);
            exam.Run();
		}

		private void DivisionOperatorsButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(DivisionTaskOperators), TaskLevel, taskCount);
            exam.Run();
		}

		private void AddFractionsButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(FractionAdditionTask), TaskLevel, taskCount);
            exam.Run();
		}

		private void SubstractFractionButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(FractionSubstractionTask), TaskLevel, taskCount);
            exam.Run();
		}

		private void MultiplyFractionButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(FractionMultiplicationTask), TaskLevel, taskCount);
            exam.Run();
		}

		private void DivisionFractionButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            // generate task
            exam = new Engine(render);
            exam.CreateTasks(typeof(FractionDivisionTask), TaskLevel, taskCount);
            exam.Run();
		}

		private void PerimeterButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            exam = new Engine(render);
            exam.CreateTasks(typeof(PerimeterTask), TaskLevel, taskCount);
            exam.Run();            
		}

		private void AreaButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            exam = new Engine(render);
            exam.CreateTasks(typeof(AreaTask), TaskLevel, taskCount);
            exam.Run();
		}

	}
}