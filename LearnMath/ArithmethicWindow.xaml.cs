﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LearnMath
{
	/// <summary>
	/// Interaction logic for ArithmethicWindow.xaml
	/// </summary>
	public partial class ArithmethicWindow : Window
	{
        public string Result { get; set; }
		public ArithmethicWindow()
		{
			this.InitializeComponent();
			
			// Insert code required on object creation below this point.
		}

		private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Close();
		}

		private void TextBoxSecond_LostFocus(object sender, System.Windows.RoutedEventArgs e)
		{
			Result = TextBoxSecond.Text;
		}

		private void TexBoxResult_LostFocus(object sender, System.Windows.RoutedEventArgs e)
		{
			Result = TexBoxResult.Text;
		}

		private void Close_Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Close();
            Result = null;
		}

		private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			this.DragMove();
		}
	}
}