﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LearnMath
{
	/// <summary>
	/// Interaction logic for ArithmethicWindow.xaml
	/// </summary>
	public partial class ArithmethicOperatorsWindow : Window
	{
        public string Result { get; set; }

		public ArithmethicOperatorsWindow()
		{
			this.InitializeComponent();
			
			// Insert code required on object creation below this point.
		}


		private void YesButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Result = "1";
			Close();
		}

		private void NoButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Result = "0";
			Close();
		}

		private void Close_Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            Close();
            Result = null;
		}

		private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			this.DragMove();
		}
	}
}