﻿#pragma checksum "..\..\ArithmethicWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "45D80AAE62036C78D0950E16ACE5B3B0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace LearnMath {
    
    
    /// <summary>
    /// ArithmethicWindow
    /// </summary>
    public partial class ArithmethicWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 7 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal LearnMath.ArithmethicWindow Window;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelScores;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelCorrectAnswer;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelTitle;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBoxFirst;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBoxSecond;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TextBoxThird;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TexBoxResult;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelSign;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LabelSign1;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OKButton;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\ArithmethicWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Close_Button;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LearnMath;component/arithmethicwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ArithmethicWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Window = ((LearnMath.ArithmethicWindow)(target));
            
            #line 9 "..\..\ArithmethicWindow.xaml"
            this.Window.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Window_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.LabelScores = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.LabelCorrectAnswer = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.LabelTitle = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.TextBoxFirst = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.TextBoxSecond = ((System.Windows.Controls.TextBox)(target));
            
            #line 23 "..\..\ArithmethicWindow.xaml"
            this.TextBoxSecond.LostFocus += new System.Windows.RoutedEventHandler(this.TextBoxSecond_LostFocus);
            
            #line default
            #line hidden
            return;
            case 8:
            this.TextBoxThird = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TexBoxResult = ((System.Windows.Controls.TextBox)(target));
            
            #line 26 "..\..\ArithmethicWindow.xaml"
            this.TexBoxResult.LostFocus += new System.Windows.RoutedEventHandler(this.TexBoxResult_LostFocus);
            
            #line default
            #line hidden
            return;
            case 10:
            this.LabelSign = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.LabelSign1 = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.OKButton = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\ArithmethicWindow.xaml"
            this.OKButton.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.Close_Button = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\ArithmethicWindow.xaml"
            this.Close_Button.Click += new System.Windows.RoutedEventHandler(this.Close_Button_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

