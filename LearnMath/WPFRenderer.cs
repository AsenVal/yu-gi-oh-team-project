﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathTaskLibrary;
using System.Windows;

namespace LearnMath
{
    public class WPFRenderer : IRenderer
    {

        public string RenderExam(string TaskTypeToString, List<string> examParametars, string scores, string previousResult)
        {

            // otvara forma i q popalva
            string result = "";
            if (TaskTypeToString == "AdditionalTaskBy" || TaskTypeToString == "AdditionalTaskFrom" ||
                TaskTypeToString == "SubstractionalTaskBy" || TaskTypeToString == "SubstractionalTaskFrom" ||
                TaskTypeToString == "MultiplicationalTaskBy" || TaskTypeToString == "MultiplicationalTaskFrom" ||
                TaskTypeToString == "DivisionTaskBy" || TaskTypeToString == "DivisionTaskFrom") // da se dobavqt
            {
                ArithmethicWindow AW = new ArithmethicWindow();
                AW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Collapsed;
                AW.LabelTitle.Content = TaskTypeToString;
                AW.LabelScores.Content = scores;
                if (previousResult != null)
                {
                    AW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Visible;
                    AW.LabelCorrectAnswer.Content = "Correct answer: " + previousResult;
                }

                AW.TextBoxFirst.Text = examParametars[0];
                AW.LabelSign.Content = examParametars[1];
                if (examParametars[2] != "?")
                {
                    AW.TextBoxSecond.Text = examParametars[2];
                    AW.TextBoxSecond.IsEnabled = false;
                    AW.TexBoxResult.IsEnabled = true;
                }
                else
                {
                    AW.TexBoxResult.Text = examParametars[examParametars.Count - 1];
                    AW.TextBoxSecond.IsEnabled = true;
                    AW.TexBoxResult.IsEnabled = false;
                }
                if (examParametars.Count == 7)
                {
                    AW.LabelSign1.Content = examParametars[3];
                    AW.TextBoxThird.Text = examParametars[4];
                }
                else
                {
                    AW.LabelSign1.Visibility = System.Windows.Visibility.Collapsed;
                    AW.TextBoxThird.Visibility = System.Windows.Visibility.Collapsed;
                }

                AW.ShowDialog();
                result = AW.Result;
            }
            else if (TaskTypeToString == "AdditionalOperators" || TaskTypeToString == "SubstractionOperators" ||
                TaskTypeToString == "MultiplicationalOperatos" || TaskTypeToString == "DivisionTaskOperators")
            {
                ArithmethicOperatorsWindow AW = new ArithmethicOperatorsWindow();
                AW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Collapsed;
                AW.LabelTitle.Content = TaskTypeToString;
                AW.LabelScores.Content = scores;
                if (previousResult != null)
                {
                    AW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Visible;
                    AW.LabelCorrectAnswer.Content = "Correct answer:" + previousResult;
                }

                AW.TextBoxFirst.Text = examParametars[0];
                AW.LabelSign.Content = examParametars[1];
                AW.TextBoxSecond.Text = examParametars[2];
                AW.LabelSign2.Content = examParametars[examParametars.Count - 2];
                AW.TexBoxResult.Text = examParametars[examParametars.Count - 1];

                if (examParametars.Count == 7)
                {
                    AW.LabelSign1.Content = examParametars[3];
                    AW.TextBoxThird.Text = examParametars[4];
                }
                else
                {
                    AW.LabelSign1.Visibility = System.Windows.Visibility.Collapsed;
                    AW.TextBoxThird.Visibility = System.Windows.Visibility.Collapsed;
                }

                AW.ShowDialog();
                result = AW.Result;
            }
            else if (TaskTypeToString == "FractionAdditionTask" || TaskTypeToString == "FractionSubstractionTask" ||
               TaskTypeToString == "FractionMultiplicationTask" || TaskTypeToString == "FractionDivisionTask") // da se dobavqt
            {
                FractionWindow FW = new FractionWindow();
                FW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Collapsed;
                FW.LabelTitle.Content = TaskTypeToString;
                FW.LabelScores.Content = scores;
                if (previousResult != null)
                {
                    FW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Visible;
                    FW.LabelCorrectAnswer.Content = "Correct answer:" + previousResult;
                }
                string[] buffer = examParametars[0].Split('/');
                FW.TextBoxFirstT.Text = buffer[0];
                FW.TextBoxFirstB.Text = buffer[1];
                FW.LabelSign.Content = examParametars[1];
                if (examParametars[2] != "?")
                {
                    buffer = examParametars[2].Split('/');
                    FW.TextBoxSecondT.Text = buffer[0];
                    FW.TextBoxSecondB.Text = buffer[1];
                    FW.TextBoxSecondT.IsEnabled = false;
                    FW.TextBoxSecondB.IsEnabled = false;
                    FW.TextBoxResultT.IsEnabled = true;
                    FW.TexBoxResultB.IsEnabled = true;
                }
                else
                {
                    buffer = examParametars[examParametars.Count - 1].Split('/');
                    FW.TextBoxResultT.Text = buffer[0];
                    FW.TexBoxResultB.Text = buffer[1];
                    FW.TextBoxSecondT.IsEnabled = true;
                    FW.TextBoxSecondB.IsEnabled = true;
                    FW.TextBoxResultT.IsEnabled = false;
                    FW.TexBoxResultB.IsEnabled = false;
                }
                FW.LabelSign2.Content = examParametars[examParametars.Count - 2];
                if (examParametars.Count == 7)
                {
                    FW.LabelSign1.Content = examParametars[3];
                    buffer = examParametars[4].Split('/');
                    FW.TextBoxThirdT.Text = buffer[0];
                    FW.TextBoxThirdB.Text = buffer[1];
                }
                else
                {
                    FW.LabelSign1.Visibility = System.Windows.Visibility.Collapsed;
                    FW.TextBoxThirdT.Visibility = System.Windows.Visibility.Collapsed;
                    FW.TextBoxThirdB.Visibility = System.Windows.Visibility.Collapsed;
                }

                FW.ShowDialog();
                result = FW.Result;
            }
            else if (TaskTypeToString == "PerimeterTask" || TaskTypeToString == "AreaTask") // da se dobavqt
            {
                GeometryWindow GW = new GeometryWindow();
                GW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Collapsed;
                GW.LabelTitle.Content = TaskTypeToString;
                GW.LabelScores.Content = scores;
                if (previousResult != null)
                {
                    GW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Visible;
                    GW.LabelCorrectAnswer.Content = previousResult;
                }
                if (examParametars[1] == "Rectangular")
                {
                    GW.CancasRectangular.Visibility = System.Windows.Visibility.Visible;
                    GW.CancasTriangular.Visibility = System.Windows.Visibility.Collapsed;
                    GW.CancasCircle.Visibility = System.Windows.Visibility.Collapsed;
                    GW.TextBoxSideAR.Text = examParametars[2];
                    GW.TextBoxSideBR.Text = examParametars[3];
                    GW.LabelPerimetarorArea.Content = examParametars[0];
                }
                else if (examParametars[1] == "Triangular")
                {
                    GW.CancasRectangular.Visibility = System.Windows.Visibility.Collapsed;
                    GW.CancasTriangular.Visibility = System.Windows.Visibility.Visible;
                    GW.CancasCircle.Visibility = System.Windows.Visibility.Collapsed;
                    GW.TextBoxSideAT.Text = examParametars[2];
                    GW.TextBoxSideBT.Text = examParametars[3];
                    GW.TextBoxSideCT.Text = examParametars[4];
                    GW.LabelPerimetarorArea.Content = examParametars[0];
                }
                else if (examParametars[1] == "Circle")
                {
                    GW.CancasRectangular.Visibility = System.Windows.Visibility.Collapsed;
                    GW.CancasTriangular.Visibility = System.Windows.Visibility.Collapsed;
                    GW.CancasCircle.Visibility = System.Windows.Visibility.Visible;
                    GW.TextBoxRadius.Text = examParametars[2];
                    GW.LabelPerimetarorArea.Content = examParametars[0];
                }

                

                GW.ShowDialog();
                result = GW.Result;
            }

            return result;
        }

        public void RenderResult(string TaskTypeToString, string scores, string previousResult)
        {
            TaskResultWindow RW = new TaskResultWindow();
            RW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Hidden;
            RW.LabelTitle.Content = TaskTypeToString;
            RW.LabelScores.Content = scores;
            if (previousResult != null)
            {
                RW.LabelCorrectAnswer.Visibility = System.Windows.Visibility.Visible;
                RW.LabelCorrectAnswer.Content = "Correct answer:" + previousResult;
            }

            RW.ShowDialog();
        }


        public void CatchErrorMessage (string message, Exception InnerException)
        {
            MessageBox.Show(message + "\n Repeat your task, you must enter real number.",
                        "Incorrect Input",
                         MessageBoxButton.OK,
                         MessageBoxImage.Warning);
        }

    }





    ///////////////////////////////


    


}
