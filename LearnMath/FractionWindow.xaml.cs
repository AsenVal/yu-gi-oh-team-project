﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LearnMath
{
	/// <summary>
	/// Interaction logic for FractionWindow.xaml
	/// </summary>
    public partial class FractionWindow : Window
    {
        public string Result { get; set; }

        public FractionWindow()
        {
            this.InitializeComponent();

            // Insert code required on object creation below this point.
        }


        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void TextBoxSecond_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            Result = String.Format("{0}/{1}", TextBoxSecondT.Text, TextBoxSecondB.Text);
        }

        private void TexBoxResult_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            Result = String.Format("{0}/{1}", TextBoxResultT.Text, TexBoxResultB.Text);
        }

        private void Close_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
            Result = null;
        }

        private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }

}