﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathTaskLibrary.Geometry
{
    [TestClass]
    public class TestTraingleTask

    {
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
        }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup()
        {
        }

        // Use TestInitialize to run code before running each test
        [TestInitialize]
        public void TestInitialize()
        {
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestCleanup()
        {
        }

        /// <summary>
        /// Test triangle Area and perimeter with some values
        /// </summary>
        [TestMethod]
        public void GeometryTaskTriangle()
        {
            Triangular triangle  = new Triangular(10, 20, 30);
            double area = triangle.CalculateArea();
            double perimeter = triangle.CalculatePerimeter();

            Console.WriteLine("Area: {0}", area);
            Console.WriteLine("Perimeter {0}", perimeter);
            
        }

        /// <summary>
        /// Test triangle Area and Perimeter with zero values
        /// </summary>
        [TestMethod]
        public void GeometryTaskTriangleZeros()
        {
            Triangular triangle = new Triangular(0, 0, 0);
            double area = triangle.CalculateArea();
            double perimeter = triangle.CalculatePerimeter();

            Console.WriteLine("Area: {0}", area);
            Console.WriteLine("Perimeter {0}", perimeter);

        }

        /// <summary>
        /// Test triangle Area and Perimeter with values less than zero
        /// </summary>
        [TestMethod]
        public void GeometryTaskTriangleLessThanZeros()
        {
            Triangular triangle = new Triangular(-1, -22, -55);
            double area = triangle.CalculateArea();
            double perimeter = triangle.CalculatePerimeter();

            Console.WriteLine("Area: {0}", area);
            Console.WriteLine("Perimeter {0}", perimeter);

        }
    }
}



