﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathTaskLibrary.Geometry
{
    [TestClass]
    public class TestPerimeterTask

    {
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
        }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup()
        {
        }

        // Use TestInitialize to run code before running each test
        [TestInitialize]
        public void TestInitialize()
        {
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestCleanup()
        {
        }

        [TestMethod]
        public void GeometryTaskRectangle()
        {
            Rectangular rectangle = new Rectangular(10,2);
            rectangle.Width = 10;
            rectangle.Height = 2;
            double area = rectangle.CalculateArea();
            double perimeter = rectangle.CalculatePerimeter();
            Console.WriteLine("Area: {0}", area);
            Console.WriteLine("Perimeter {0}", perimeter);
        }

       
    }
}



