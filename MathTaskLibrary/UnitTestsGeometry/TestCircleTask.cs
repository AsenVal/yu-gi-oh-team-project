﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Geometry
{
    [TestClass]
    public class TestCircleTask
    {
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
        }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup()
        {
        }

        // Use TestInitialize to run code before running each test
        [TestInitialize]
        public void TestInitialize()
        {
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestCleanup()
        {
        }

        /// <summary>
        /// Just test Area and Perimeter of Circle with some value 
        /// </summary>
        [TestMethod]
        public void GeometryTaskCircle()
        {
            Circle circle = new Circle(3);

            double area = circle.CalculateArea();
            Console.WriteLine("Area: {0}", area);

            double perimeter = circle.CalculatePerimeter();
            Console.WriteLine("Perimeter: {0}", perimeter);
        }


        /// <summary>
        /// Test Circle area and perimeter with zero
        /// </summary>
        [TestMethod]
        public void GeometryTaskCircleZero()
        {
            Circle circle = new Circle(0);

            double area = circle.CalculateArea();
            Console.WriteLine("Area: {0}", area);

            double perimeter = circle.CalculatePerimeter();
            Console.WriteLine("Perimeter: {0}", perimeter);
        }

        /// <summary>
        /// Test Area and Perimeter with parameter less than zero 
        /// </summary>
        [TestMethod]
        public void GeometryTaskCircleLessThanZero()
        {
            Circle circle = new Circle(-8888);

            double area = circle.CalculateArea();
            Console.WriteLine("Area: {0}", area);

            double perimeter = circle.CalculatePerimeter();
            Console.WriteLine("Perimeter: {0}", perimeter);
        }
    }
}