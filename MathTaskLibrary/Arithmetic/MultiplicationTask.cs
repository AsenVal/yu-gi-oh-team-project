﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
    public class Multiplication : ArithmeticTask
    {
        //constructor SubsractionTask
        public Multiplication(Difficulty level)
            : base(level)
        {
            base.firstMathOperator = "*";
           
        }

        public virtual void GenerateNumbersForMultiple()
        {
        }

        
    }
}

