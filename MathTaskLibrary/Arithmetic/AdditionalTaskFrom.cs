﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
    public class AdditionalTaskFrom : AdditionTask
    {
        public AdditionalTaskFrom(Difficulty level)
            : base(level)
        {
            base.secondMathOperator = "=";
        }

        //GenerateNumbersForAddition method
        public override void GenerateNumbersForAddition()   // Todo mislq 4e raboti no trqbva da se testva
        {
            int first = RandomProvider.Next(1, 20);
            int second = RandomProvider.Next(1, 20);
            int sum;
            //check level is normal
            if ((int)base.Level == 0)
            {
                sum = first + second;
                base.firstNumber = first;
                base.secondNumber = sum;
                base.result = second.ToString();
            }

             //check level is difficult or not 
            else if ((int)base.Level == 1)
            {
                int third = RandomProvider.Next(1, 30);
                sum = first + second + third;
                base.firstNumber = first;
                base.secondNumber = sum;
                base.thirdNumber = third;
                base.result = second.ToString();

            }
        }
        //2 + ? = 4     2 is first 4 is second ? is result
        public override List<string> GenerateTask()
        {
            GenerateNumbersForAddition();
            List<string> returnString = new List<string>();
            returnString.Add(firstNumber.ToString());
            returnString.Add(firstMathOperator);
            returnString.Add("?");   
            if (base.Level == Difficulty.difficult)
            {
                returnString.Add(firstMathOperator); 
                returnString.Add(thirdNumber.ToString());
            }
            returnString.Add(secondMathOperator);
            returnString.Add(secondNumber.ToString()); // razmeneni redove
            return returnString;
        }

    }
}
