﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
  public class MultiplicationalTaskBy : Multiplication
    {
        public MultiplicationalTaskBy(Difficulty level)
            : base(level)
        {
            base.secondMathOperator = "=";
        }

        public override void GenerateNumbersForMultiple()   // Todo mislq 4e raboti no trqbva da se testva
        {
            base.firstNumber = RandomProvider.Next(1, 12);
            base.secondNumber = RandomProvider.Next(0, 10);

            if ((int)base.Level == 0)
            {
                base.result = Convert.ToString(base.firstNumber * base.secondNumber);
            }

            else if ((int)base.Level == 1)
            {
                base.thirdNumber = RandomProvider.Next(1, 4);
                base.result = Convert.ToString(base.firstNumber * base.secondNumber * base.thirdNumber);

            }
        }

        //3 * 5 = ?
        
        //override method 
        public override List<string> GenerateTask()
        {
            GenerateNumbersForMultiple();
            return base.GenerateTask();
        }
    }
}
