﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
    public class SubstractionalTaskBy : SubstractionTask
    {
        public SubstractionalTaskBy(Difficulty level)
            : base(level)
        {
            base.secondMathOperator = "=";
        }

        public override void GenerateNumbersForSubstraction()   // Todo mislq 4e raboti no trqbva da se testva
        {
            base.firstNumber = RandomProvider.Next(10, 30);
            base.secondNumber = RandomProvider.Next(1, firstNumber + 1);

            if ((int)base.Level == 0)
            {
                base.result = Convert.ToString(base.firstNumber - base.secondNumber);
            }

            else if ((int)base.Level == 1)
            {
                base.thirdNumber = RandomProvider.Next(0, base.firstNumber - base.secondNumber + 1);
                base.result = Convert.ToString(base.firstNumber - base.secondNumber - base.thirdNumber);

            }
        }

        //20 - 5 = ?

        
        //override method 
        public override List<string> GenerateTask()
        {
            GenerateNumbersForSubstraction();
            return base.GenerateTask();
        }
    }
}
