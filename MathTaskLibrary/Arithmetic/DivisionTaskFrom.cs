﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
    public class DivisionTaskFrom : DivisionTask 
    {
        public DivisionTaskFrom(Difficulty level)
            : base(level)
        {
            this.secondMathOperator = "=";
        }

        public override void GenerateNumbersForDivision()
        {
            int first = RandomProvider.Next(1, 12);
            int second = RandomProvider.Next(1, 12);
            if ((int)base.Level == 0)
            {
                base.firstNumber = first * second;
                base.secondNumber = second;
                base.result = first.ToString();
            }
            else if ((int)base.Level == 1)
            {
                int third = RandomProvider.Next(1, 4);
                base.firstNumber = first * second * third;
                base.secondNumber = second;
                base.thirdNumber = third;
                base.result = first.ToString();
            }
        }

        //20 / ? = 4         

        public override List<string> GenerateTask()
        {
            GenerateNumbersForDivision();
            List<string> returnString = new List<string>();
            returnString.Add(firstNumber.ToString());
            returnString.Add(firstMathOperator);
            returnString.Add("?");
            
                                                   
            if (base.Level == Difficulty.difficult)
            {
                returnString.Add(firstMathOperator); 
                returnString.Add(thirdNumber.ToString());
            }
            returnString.Add(secondMathOperator);
            returnString.Add(secondNumber.ToString()); // razmeneni redove
            return returnString;
        }

       
    }
}
