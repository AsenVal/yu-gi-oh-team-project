﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
   public class SubstractionalTaskFrom : SubstractionTask
    {
        public SubstractionalTaskFrom(Difficulty level)
            : base(level)
        {
            base.secondMathOperator = "=";
        }

        public override void GenerateNumbersForSubstraction()   // Todo mislq 4e raboti no trqbva da se testva
        {
            int first = RandomProvider.Next(10, 30);
            int second = RandomProvider.Next(1, firstNumber + 1);
            int substract;

            if ((int)base.Level == 0)
            {
                substract = first - second;
                base.firstNumber = first;
                base.secondNumber = substract;
                base.result = second.ToString();
            }

            else if ((int)base.Level == 1)
            {
                int third = RandomProvider.Next(0, first - second + 1);
                substract = first - second - third;
                base.firstNumber = first;
                base.secondNumber = substract;
                base.thirdNumber = third;
                base.result = second.ToString();

            }
        }

        //6 - ? = 4       
        public override List<string> GenerateTask()
        {
            GenerateNumbersForSubstraction();
            List<string> returnString = new List<string>();
            returnString.Add(firstNumber.ToString());
            returnString.Add(firstMathOperator);
            returnString.Add("?");               // razmeneni redove
            if (base.Level == Difficulty.difficult)
            {
                returnString.Add(firstMathOperator); 
                returnString.Add(thirdNumber.ToString());
            }
            returnString.Add(secondMathOperator);
            returnString.Add(secondNumber.ToString()); // razmeneni redove
            return returnString;
        }

    }
}
