﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
    public class SubstractionTask : ArithmeticTask
    {

        //constructor SubsractionTask
        public SubstractionTask(Difficulty level)
            : base(level)
        {
            base.firstMathOperator = "-";
           
        }

        public virtual void GenerateNumbersForSubstraction()
        {
        }

    }
}
