﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
    public class MultiplicationalTaskFrom : Multiplication
    {
         public MultiplicationalTaskFrom(Difficulty level)
            : base(level)
        {
            this.secondMathOperator = "=";
        }

         public override void GenerateNumbersForMultiple()   // Todo mislq 4e raboti no trqbva da se testva
         {
             int first = RandomProvider.Next(1, 12);
             int second = RandomProvider.Next(0, 10);
             int multiple;

             if ((int)base.Level == 0)
             {
                 multiple = first * second;
                 base.firstNumber = first;
                 base.secondNumber = multiple;
                 base.result = second.ToString();
             }

             else if ((int)base.Level == 1)
             {
                 int third = RandomProvider.Next(1, 4);
                 multiple = first * second * third;
                 base.firstNumber = first;
                 base.secondNumber = multiple;
                 base.thirdNumber = third;
                 base.result = second.ToString();

             }
         }

        //6 *? = 18
      
        public override List<string> GenerateTask()
        {
            GenerateNumbersForMultiple();
            List<string> returnString = new List<string>();
            returnString.Add(firstNumber.ToString());
            returnString.Add(firstMathOperator);
            returnString.Add("?");                   // razmeneni redove
            if (base.Level == Difficulty.difficult)
            {
                returnString.Add(firstMathOperator); 
                returnString.Add(thirdNumber.ToString());
            }
            returnString.Add(secondMathOperator);
            returnString.Add(secondNumber.ToString()); // razmeneni redove
            return returnString;
        }

    }
}
