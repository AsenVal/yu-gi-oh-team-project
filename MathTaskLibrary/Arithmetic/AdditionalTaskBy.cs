﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
    public class AdditionalTaskBy : AdditionTask
    {       
        public AdditionalTaskBy(Difficulty level)
            : base(level)
        {
            base.secondMathOperator = "=";
        }


        //GenerateNumbersForAddition method
        public override void GenerateNumbersForAddition()   // Todo mislq 4e raboti no trqbva da se testva
        {
            base.firstNumber = RandomProvider.Next(1, 20);
            base.secondNumber = RandomProvider.Next(1, 20);

            //check level is normal
            if ((int)base.Level == 0)
            {
                base.result = Convert.ToString(base.firstNumber + base.secondNumber);
            }

             //check level is difficult or not 
            else if ((int)base.Level == 1)
            {
                base.thirdNumber = RandomProvider.Next(1, 30);
                base.result = Convert.ToString(base.firstNumber + base.secondNumber + base.thirdNumber);

            }
        }
        //20 + 5 = ?

        //override method 
        public override List<string> GenerateTask()
        {
            GenerateNumbersForAddition();
            return base.GenerateTask();
        }
    }
}
