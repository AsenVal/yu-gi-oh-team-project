﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
   public class DivisionTaskBy : DivisionTask
    {        
        public DivisionTaskBy(Difficulty level)
            : base(level)
        { 
            base.secondMathOperator = "=";
        }
       
        public override void GenerateNumbersForDivision()
        {
            int first = RandomProvider.Next(1, 12);
            int second = RandomProvider.Next(1, 12);
            if ((int)base.Level == 0)
            {
                base.firstNumber = first * second;
                base.secondNumber = first;
                base.result = second.ToString();
            }
            else if ((int)base.Level == 1)
            {
                int third = RandomProvider.Next(1, 4);
                base.firstNumber = first * second * third;
                base.secondNumber = first;
                base.thirdNumber = second;
                base.result = third.ToString();
            }
        }
        //20 / 5 = ?
       
        public override List<string> GenerateTask()
        {
            GenerateNumbersForDivision();
            return base.GenerateTask(); // za6toto e sa6tata
        }

        
    }
}
