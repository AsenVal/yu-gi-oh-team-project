﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Arithmetic
{
    public class SubstractionOperators : SubstractionTask, ICompareNumbers
    {
        private int resultNumber;

        public SubstractionOperators(Difficulty level)
            : base(level)
        {
        }



        //15 - 4 > 4
        public override void GenerateNumbersForSubstraction()   // Todo mislq 4e raboti no trqbva da se testva
        {
            base.secondMathOperator = base.generateSign(">", "<");
            base.firstNumber = RandomProvider.Next(10, 30);
            base.secondNumber = RandomProvider.Next(1, firstNumber + 1);
            int randomNumber; 
            do
            {
                randomNumber = RandomProvider.Next(-2, 3);
            }
            while (randomNumber == 0);
            if ((int)base.Level == 0)
            {               
                resultNumber = base.firstNumber - base.secondNumber + randomNumber;
                if ((CompareTwoSides<int>(firstNumber - secondNumber, resultNumber, false) && secondMathOperator == "<")
                    || (CompareTwoSides<int>(firstNumber - secondNumber, resultNumber, true) && secondMathOperator == ">"))
                {
                    base.result = Convert.ToString(1);
                }
                else
                {
                    base.result = Convert.ToString(0);
                }
            }
            else if ((int)base.Level == 1)
            {
                base.thirdNumber = RandomProvider.Next(0, base.firstNumber - base.secondNumber + 1);
                resultNumber = base.firstNumber - base.secondNumber - base.thirdNumber + randomNumber;
                if ((CompareTwoSides<int>(firstNumber - secondNumber - thirdNumber, resultNumber, false) && secondMathOperator == "<")
                    || (CompareTwoSides<int>(firstNumber - secondNumber - thirdNumber, resultNumber, true) && secondMathOperator == ">"))
                {
                    base.result = Convert.ToString(1);
                }
                else
                {
                    base.result = Convert.ToString(0);
                }
            }
        }

        public override List<string> GenerateTask()
        {
            GenerateNumbersForSubstraction();
            List<string> returnString = new List<string>();
            returnString.Add(firstNumber.ToString());
            returnString.Add(firstMathOperator);
            returnString.Add(secondNumber.ToString());
            if (Level == Difficulty.difficult)
            {
                returnString.Add(firstMathOperator);
                returnString.Add(thirdNumber.ToString());
            }
            returnString.Add(secondMathOperator);
            returnString.Add(resultNumber.ToString());
            return returnString;
        }

        public bool CompareTwoSides<T>(T leftSide, T rightSide, bool isLarger) where T : IComparable
        {
            if (isLarger)
            {
                if (leftSide.CompareTo(rightSide) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (leftSide.CompareTo(rightSide) < 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }           
        }

        public override string ToString()
        {
            string sign;
            if (resultNumber > Convert.ToInt32(base.result))
            {
                sign = ">";
            }
            else
            {
                sign = "<";
            }

            if ((int)base.Level == 0)
            {
                return String.Format("{0} {1} {2} {3} {4}", base.firstNumber, base.firstMathOperator, base.secondNumber, sign, base.result);
            }
            else
            {
                return String.Format("{0} {1} {2} {3} {4} {5} {6} ", base.firstNumber, base.firstMathOperator, base.secondNumber, base.firstMathOperator, base.thirdNumber, sign, base.result);
            }
        }
    }
}
