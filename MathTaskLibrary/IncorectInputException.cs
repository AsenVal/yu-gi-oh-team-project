﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathTaskLibrary
{
    public class IncorectInputException : Exception
    {
        // Constructor
        public IncorectInputException(string message)
            :base(message)
        {
        }
        public IncorectInputException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
