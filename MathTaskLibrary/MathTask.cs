﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathTaskLibrary.Geometry;

namespace MathTaskLibrary
{
    public abstract class MathTask : IExamable
    {
        //fields
        protected int firstNumber;
        protected int secondNumber;
        protected int thirdNumber;
        public string result {get; set;}
        public Difficulty Level { get; private set; }
        

        // moje i da ne sa tuk zavisi ot decata
        public string firstMathOperator { get; set; }
        public string secondMathOperator { get; set; }
        //
        
        //costructor
        public MathTask(Difficulty level)
        {
            this.Level = level;
        }


        public virtual string generateSign(string first, string second)
        {
            if (RandomProvider.Next(0, 2) == 0)
            {
                return first;
            }
            else
            {
                return second;
            }
        }


        // Alternative
        //public virtual Task GenerateTask()
        //{
        //    return new Task();
        //}

        public virtual List<string> GenerateTask() // trqbva da se implementira ot decata.
        {
            // primerna funkcia za decata, ako ne vi otgovarq go override-vate
            // ne trqbva da q ima tuk, posle 6te se iztrie

            List<string> returnString = new List<string>();
            returnString.Add(firstNumber.ToString());
            returnString.Add(firstMathOperator);
            returnString.Add(secondNumber.ToString());
            if (Level == Difficulty.difficult)
            {
                returnString.Add(firstMathOperator);
                returnString.Add(thirdNumber.ToString());
            }
            returnString.Add(secondMathOperator);
            returnString.Add("?");
            return returnString;

        }
       

        /*
        //optional MathOperator
        public int CreateMathOperator()
        {
            Random random = new Random();
            firstMathOperator = random.Next(1, 5);
            return firstMathOperator;
        }*/

        public virtual string CalculateResult()
        {
            return result;
        }

        public virtual bool CheckResult(string userResult)
        {

            // primerna funkcia za decata ako rezultata e 4isla
            // ne trqbva da q ima tuk, posle 6te se iztrie

            int checkNumber;
            if (int.TryParse(userResult, out checkNumber))
            {
                return checkNumber == Convert.ToInt32(this.result);
            }
            else
            {
                throw new IncorectInputException("You must input a number");
            }
        }

        public override string ToString()
        {

            if ((int)this.Level == 0)
            {
                return String.Format("{0} {1} {2} {3} {4}", this.firstNumber, this.firstMathOperator, this.secondNumber, this.secondMathOperator, this.result);
            }
            else
            {
                return String.Format("{0} {1} {2} {3} {4} {5} {6} ", this.firstNumber, this.firstMathOperator, this.secondNumber, this.firstMathOperator, this.thirdNumber, this.secondMathOperator, this.result);
            }
        }
    }
}
