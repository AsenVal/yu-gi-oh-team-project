﻿using MathTaskLibrary.Arithmetic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace MathTaskLibrary
{
    [TestClass]
    public class TestSubstractionTask
    {
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
        }

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ClassCleanup()
        {
        }

        // Use TestInitialize to run code before running each test
        [TestInitialize]
        public void TestInitialize()
        {
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void TestCleanup()
        {
        }

        [TestMethod]
        public void TestAdditionTask()
        {
            SubstractionTask task = new SubstractionTask(0);
            List<string> test =  task.GenerateTask();

            foreach (var item in test)
            {
                Console.WriteLine(item);
            }
        }

        [TestMethod]
        public void TestAdditionTaskDifficult()
        {
            SubstractionTask task = new SubstractionTask(Difficulty.difficult);
            List<string> test = task.GenerateTask();

            foreach (var item in test)
            {
                Console.WriteLine(item);
            }
        }
       
    }
}



