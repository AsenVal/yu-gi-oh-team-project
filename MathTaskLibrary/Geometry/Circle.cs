﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Geometry
{
    public class Circle:Shape
    {

        public Circle(int radius)
            : base(radius, radius)
        { }

        
        // calculate length and area by given radius
        public override double CalculateArea()
        {
            if (this.Width < 0)
            {
                throw new System.ArgumentException("radius cannot be less than zero");
            }
            return this.Width * this.Width * Math.PI;
        }

        public override double CalculatePerimeter()
        {
            if (this.Width < 0)
            {
                throw new System.ArgumentException("radius cannot be less than zero");
            }
            return Math.PI * this.Width * 2;
        }

        public override string ToString()
        {
            string text = String.Format
                ("of this circle with given dimensions:\nRadius:{0}",
                this.Width);
            return text;
        }

    }
}
