﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Geometry
{
    public abstract class Shape
    {
        #region Fields

        private int width;
        private int height;

        #endregion

        public Shape(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        #region Properties

        public int Width
        {
            get
            {
                return this.width;
            }
            set
            {
                this.width = value;
            }
        }

        public int Height
        {
            get
            {
                return this.height;
            }
            set
            {
                this.height = value;
            }
        }

        #endregion

        #region Methods

        public abstract double CalculateArea();
        public abstract double CalculatePerimeter();

        #endregion


    }
}
