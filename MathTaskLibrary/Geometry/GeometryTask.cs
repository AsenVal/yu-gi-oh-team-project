﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathTaskLibrary.Geometry
{
    public abstract class GeometryTask : MathTask
    {
        public Shape ChosenShape { get; set; }
        //public TaskType Type { get; set; }

        public GeometryTask(Difficulty level)
            :base(level)
        {
            this.CreateShape();
        }

        //alternative
        //public abstract Task GenerateTask();


        public void CreateShape()
        {
            int choise = RandomProvider.Next(0,4);
            int a, b, c;
            if((int)base.Level == 0)
            {
                a = RandomProvider.Next(4,15);
                b = RandomProvider.Next(4,15);
                c = RandomProvider.Next(Math.Min(a,b),Math.Max(a, b) + 1);
            }
            else
            {
                a = RandomProvider.Next(9,25);
                b = RandomProvider.Next(9, 25);
                c = RandomProvider.Next(Math.Min(a, b), Math.Max(a, b) + 1);
            }
            if (choise == 0)
            {
                this.ChosenShape = new Rectangular(a, b);
            }
            else if (choise == 1)
            {
                this.ChosenShape = new Triangular(a, b, c);
            }
            else
            {
                this.ChosenShape = new Circle(a);
            }
        }

    }
}
