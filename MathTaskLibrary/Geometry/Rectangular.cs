﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Geometry
{
    public class Rectangular : Shape
    {
        //public int sideA { set; get; }
        //public int sideB { set; get; }

        public Rectangular( int a, int b)
            :base(a, b)
        {
            //to do exceptions for <0
        }

        public override double CalculatePerimeter()
        {
            return 2 * (base.Width + base.Height);
        }

        public override double CalculateArea()
        {
            return base.Width * base.Height;
        }

        //public override string GetName()
        //{
        //    return "rectangular";
        //}

        public override string ToString()
        {
            string text = String.Format
                ("of this rectangular shape with given dimensions:\nWidth:{0} \nHeight:{1}",
                this.Width, this.Height);
            return text;
        }
    }
}
