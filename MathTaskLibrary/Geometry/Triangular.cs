﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Geometry
{
    public class Triangular : Shape
    {
        public int ThirdSide { get; set; }

        public Triangular(int first, int second, int third)
            : base(first, second)
        {

            if (ValidTriangle(first, second, third))
            {
                this.ThirdSide = third;
            }
            else
            {
                this.ThirdSide = Math.Max(first, second);
            }
        }


        public override double CalculateArea()
        {
            double p = (this.Width + this.Height + this.ThirdSide);
            return Math.Sqrt(p * (p - this.Width) * (p - this.Height) * (p - this.ThirdSide));
        }

        public override double CalculatePerimeter()
        {
            return this.Width + this.Height + this.ThirdSide;
        }

        private bool ValidTriangle(int sideA, int sideB, int sideC)
        {
            bool result = true;

            if (sideA <= 0 || sideB <= 0 || sideC <= 0)
            {
                result = false;
            }
            bool triangleInequality = sideA < sideB + sideC &&
                                      sideB < sideA + sideC &&
                                      sideC < sideB + sideA;
            if (triangleInequality)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public override string ToString()
        {
            string text = String.Format
                          ("of this triangular shape with given dimensions:\nFirst side:{0} \nsecond side:{1} \nthird side:{2}",
                          this.Width, this.Height, this.ThirdSide);
            return text;
        }

        //TODO
    }   //Calculate Area by given side and hight for normal difficulty
        //make the calculation of area by 3 sides for hard difficulty
}
