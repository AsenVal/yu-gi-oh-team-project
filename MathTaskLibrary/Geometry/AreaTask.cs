﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Geometry
{
    public class AreaTask : GeometryTask
    {
        public AreaTask(Difficulty level)
            : base(level)
        { }

        public override List<string> GenerateTask()
        {
            List<string> myTask = new List<string>();

            base.CreateShape();
            base.result = Math.Round(this.ChosenShape.CalculateArea(),0).ToString();

            myTask.Add("Calculate the area ");
            myTask.Add(this.ChosenShape.GetType().Name);  
            myTask.Add(this.ChosenShape.Width.ToString());
            myTask.Add(this.ChosenShape.Height.ToString());

            if (this.ChosenShape is Triangular)
            {
                myTask.Add((this.ChosenShape as Triangular).ThirdSide.ToString());
            }
            else
            {
                myTask.Add(""); // to indentify if not in triangle.
            }
           
            myTask.Add("?");
            return myTask;
        }

        public override string ToString()
        {
            if (this.ChosenShape is Rectangular)
            {
                return String.Format("Area of {0} with sides a={1}, b={2} is {3}", this.ChosenShape.GetType().Name, this.ChosenShape.Width, this.ChosenShape.Height, base.result);
            }
            else if (this.ChosenShape is Triangular)
            {
                return String.Format("Area of {0} with sides a={1}, b={2}, c={3} is {4}", this.ChosenShape.GetType().Name, this.ChosenShape.Width, this.ChosenShape.Height, (this.ChosenShape as Triangular).ThirdSide, base.result);
            }
            else
            {
                return String.Format("Area of {0} with radius r={1} is {2}", this.ChosenShape.GetType().Name, this.ChosenShape.Width, base.result);
            }
        }

    }
}