﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary
{
    public interface IExamable
    {
        List<string> GenerateTask();
        string CalculateResult();
        bool CheckResult(string UserResult);
    }
}
