﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary
{
    public interface IRenderer
    {
        //Todo Irenderer
        //void EnqueueForRendering(IRenderable obj);

        string RenderExam(string TaskTypeToString, List<string> examParametars, string scores, string previousTask); // parametrite za i-tata zadavha i resultata za prednata

        void RenderResult(string TaskTypeToString, string scores, string previousResult); // rezultata za prednata
        void CatchErrorMessage(string message, Exception InnerException);

        //void RenderAll();

        //void ClearQueue();

    }
}
