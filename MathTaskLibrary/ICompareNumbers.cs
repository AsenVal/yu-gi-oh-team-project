﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary
{
    public interface ICompareNumbers
    {
        bool CompareTwoSides<T>(T leftSide, T rightSide, bool isLarger) where T : IComparable; // isLarger means '>' a leftSide and right side means from its both sides
    }
}
