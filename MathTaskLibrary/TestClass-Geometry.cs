﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MathTaskLibrary.Geometry;

namespace MathTaskLibrary
{
    //This class should not be included to project
    //Testing Geometry module
    class TestClassGeometry
    {
        public void Main()
        {
            List<GeometryTask> tasks = new List<GeometryTask>();
            tasks.Add(new PerimeterTask(Difficulty.normal));
            tasks.Add(new AreaTask(Difficulty.difficult));

            List<List<string>> taskList = new List<List<string>>();

            foreach (var item in tasks)
            {
                List<string> generated = item.GenerateTask();
                taskList.Add(generated);
            }

            for (int i = 0; i < taskList.Count; i++)
            {
                for (int j = 0; j < taskList[i].Count; i++)
                {
                    Console.WriteLine(taskList[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}
