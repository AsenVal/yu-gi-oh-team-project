﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Other
{
    public class FractionTask : MathTask
    {
        private int firstDenominator;
        private int secondDenominator;
        protected Fraction f1;
        protected Fraction f2;
        protected Fraction f3;
        public Difficulty FractionLevel { get; private set; }
        public int FirstNumerator { get; private set; }
        public int SecondNumerator { get; private set; }
        
        public int FirstDenominator
        {
            get { return this.firstDenominator; }
            set
            {
                if (value == 0)
                {
                    throw new ArithmeticException("The denominator of any fraction cannot have the value zero");
                }
                else
                {
                    this.firstDenominator = value;
                }

            }
        }
        public int SecondtDenominator
        {
            get { return this.secondDenominator; }
            set
            {
                if (value == 0)
                {
                    throw new ArithmeticException("The denominator of any fraction cannot have the value zero");
                }
                else
                {
                    this.secondDenominator = value;
                }

            }
        }

        // Constructor
        public FractionTask(Difficulty level)
            : base(level)
        {
            this.FractionLevel = level;
        }

        public virtual void GenerateNumbersForFraction()
        {
            int numerator = RandomProvider.Next(1, 11);
            int denominator = RandomProvider.Next(numerator, 21);
            f1 = new Fraction(numerator, denominator);
            numerator = RandomProvider.Next(1, 11);
            denominator = RandomProvider.Next(numerator, 21);
            f2 = new Fraction(numerator, denominator);
            if ((int)FractionLevel == 1)
            {
                numerator = RandomProvider.Next(1, 11);
                denominator = RandomProvider.Next(numerator, 21);
                f3 = new Fraction(numerator, denominator);
            }
        }

        public override List<string> GenerateTask()
        {
            GenerateNumbersForFraction();
            List<string> returnString = new List<string>();
            returnString.Add(f1.ToString());
            returnString.Add(firstMathOperator);
            returnString.Add(f2.ToString());
            if (Level == Difficulty.difficult)
            {
                returnString.Add(firstMathOperator);
                returnString.Add(f3.ToString());
            }
            returnString.Add(secondMathOperator);
            returnString.Add("?");
            return returnString;
        }

        // da se vidi dali nqma da otide nagore po darvoto ako e ednakvo
        public override bool CheckResult(string userResult) 
        {
            int checkNumerator;
            int checkDenominator;
            string[] buffer = userResult.Split('/');
            if (int.TryParse(buffer[0], out checkNumerator) && int.TryParse(buffer[1], out checkDenominator))
            {
                return String.Format("{0}/{1}", checkNumerator, checkDenominator) == base.result;
            }
            else
            {
                throw new IncorectInputException("You must input a number");
            }
        }

        
    }
}

