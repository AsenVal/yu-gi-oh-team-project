﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary.Other
{
    public class FractionMultiplicationTask : FractionTask
    {
        public FractionMultiplicationTask(Difficulty level)
            : base(level)
        {
            base.firstMathOperator = "*";
            base.secondMathOperator = "=";
        }

        public override void GenerateNumbersForFraction()
        {
            base.GenerateNumbersForFraction();
            if ((int)base.Level == 0)
            {
                base.result = Convert.ToString(f1 * f2);
            }
            else
            {
                base.result = Convert.ToString(f1 * f2 * f3);
            }
        }
    }
}
