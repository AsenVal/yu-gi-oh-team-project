﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary
{
    public class Scores
    {
        //fields
        private double correct;
        private double wrong;

        //properties
        public double Correct
        {
            get { return correct; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Correct counter must be positive number");
                }
                else
                {
                    correct = value;
                }
            }
        }
        public double Wrong
        {
            get { return wrong; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Wrong counter must be positive number");
                }
                else
                {
                    wrong = value;
                }
            }
        }
        public double Percentage { get; private set; }

        // Constructor
        public Scores()
        {
            this.Correct = 0;
            this.Wrong = 0;
        }

        public override string ToString()
        {
            if ((Correct + Wrong) == 0)
            {
                Percentage = 0;
            }
            else
            {
                Percentage = Correct / (Correct + Wrong);
            }
            return String.Format("Your Scores - Corect:{0}  Wrong:{1}  Percentage{2:P2}", Correct, Wrong, Percentage);
        }
    }
}
