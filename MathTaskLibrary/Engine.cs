﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MathTaskLibrary
{



    public class Engine : ITaskable
    {
        //Fields
        private IRenderer renderer;
        private List<MathTask> examTasks;
        private List<string> examParametars;

        // Constructor must inicialize all lists
        public Engine(IRenderer renderer)
        {
            this.renderer = renderer;
            this.examTasks = new List<MathTask>();
        }

        public virtual void CreateTasks(Type mathTaskType, Difficulty level, int numberOfTasks)
        {
            //Todo v zavisimost ot tipa vra6ta 10 zadachi i gi dobavq s AddObject v examtasks
            // razlichia pri razlicnite tipove dali i kakvi, ako e po goren klas random ot decata
            // cikal do broq zada4i ili samo vra6ta tip, neznam o6te

            object[] args = new object[] { level };
            for (int i = 0; i < numberOfTasks; i++)
            {
                examTasks.Add((MathTask)Activator.CreateInstance(mathTaskType, args));
            }
        }

        public virtual void Run()
        {

            string previousTask = null;
            string UserResult;
            Scores scores = new Scores();
            for (int i = 0; i < examTasks.Count; i++)
            {
                // da se korigira v zavisimost ot ChooseExam
                try
                {
                    examParametars = examTasks[i].GenerateTask();
                    UserResult = renderer.RenderExam(examTasks[i].GetType().Name, examParametars, scores.ToString(), previousTask);
                    if (UserResult == null)
                    {
                        break;
                    }
                    if (examTasks[i].CheckResult(UserResult))
                    {
                        previousTask = null;
                        scores.Correct++;
                    }
                    else
                    {
                        //previousTask = PreviousResult();
                        previousTask = examTasks[i].ToString();
                        scores.Wrong++;
                    }
                }
                catch (IncorectInputException IIE)
                {
                    i--;
                    renderer.CatchErrorMessage(IIE.Message, IIE);
                }
            }
            renderer.RenderResult(examTasks[examTasks.Count - 1].GetType().Name, scores.ToString(), previousTask);
            examTasks.Clear(); // clear tasks
            //renderer.RenderAll();

        }

        /*private string PreviousResult()   // da se opravi za geometria
        { 
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < examParametars.Count; i++)
            {
                if (examParametars[i] != "?")
                {
                    sb.Append(examParametars[i]);
                    sb.Append(" ");
                }
                else
                {
                    sb.Append(examTasks[i].CalculateResult());
                }
            }

            return sb.ToString();
        }*/


    }
}
