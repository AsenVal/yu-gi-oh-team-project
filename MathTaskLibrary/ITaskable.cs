﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathTaskLibrary
{
    public interface ITaskable
    {
        void CreateTasks(Type mathTaskType, Difficulty level, int numberOfTasks);

        void Run();
    }
}
